package net.battleships.game.exception

class ResourceNotFoundException(message: String) : RuntimeException(message)
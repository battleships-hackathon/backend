package net.battleships.game.api.advice

import net.battleships.game.exception.ResourceNotFoundException
import net.battleships.game.utils.LoggerDelegate
import org.slf4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class GlobalExceptionHandler {
    private val logger: Logger by LoggerDelegate()

    @ExceptionHandler(ResourceNotFoundException::class)
    fun handleResourceNotFoundException(ex: ResourceNotFoundException, request: WebRequest): ResponseEntity<String> {
        logger.error("Request processing failed: $request", ex)

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(ex.message)
    }

    @ExceptionHandler(Exception::class)
    fun handleDomainWriteException(ex: Exception, request: WebRequest): ResponseEntity<String> {
        logger.error("Request processing failed: $request", ex)

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Error: Unable to handle request")
    }

}
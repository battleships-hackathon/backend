package net.battleships.game.api.v1

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/hello")
class HelloController {
    @GetMapping
    fun hello(): String {
        val authentication: Authentication = SecurityContextHolder.getContext().authentication
        return "Oh, hello ${authentication.principal}!"
    }
}
package net.battleships.game.api.v1

import net.battleships.game.domain.model.AuthToken
import net.battleships.game.domain.service.UserDomainService
import net.battleships.game.security.token.TokenService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/v1/signin")
class SignInController(private val tokenService: TokenService,
                       private val userDomainService: UserDomainService) {
    @PostMapping
    fun login(@RequestParam name: String, @RequestParam host: String): AuthToken {
        val token: AuthToken = tokenService.generateToken()
        userDomainService.saveUser(name, host, token)

        return token
    }
}
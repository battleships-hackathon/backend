package net.battleships.game.domain.mapper

import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.domain.model.IdOfUser
import net.battleships.game.domain.model.TurnHistory
import net.battleships.game.domain.model.TurnResult
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.springframework.stereotype.Repository

@Mapper
@Repository
interface TurnHistoryMapper {

    // language=sql
    @Select("""SELECT th.game_id gameId, 
                      th.user_id userId,
                      th.number number,
                      th.abscissa abscissa,
                      th.ordinate ordinate,
                      th.result result
                 FROM turn_history th
                WHERE th.game_id = #{gameId}
             ORDER BY th.number;""")
    fun listTurnHistory(@Param("gameId") gameId: IdOfGame): MutableList<TurnHistory>

    // language=sql
    @Insert("""INSERT INTO turn_history (game_id, number, user_id, abscissa, ordinate, result) 
               VALUES (#{gameId}, next_game_turn_number(#{gameId}), #{userId}, #{abscissa}, #{ordinate}, #{result});""")
    fun insertTurnHistory(@Param("gameId") gameId: IdOfGame,
                          @Param("userId") userId: IdOfUser,
                          @Param("abscissa") abscissa: Int,
                          @Param("ordinate") ordinate: Int,
                          @Param("result") result: TurnResult)
}
package net.battleships.game.domain.mapper

import net.battleships.game.domain.model.AuthToken
import net.battleships.game.domain.model.User
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.springframework.stereotype.Repository

@Mapper
@Repository
interface UserMapper {

    // language=sql
    @Select("""SELECT u.id, u.name, u.host, u.token 
                 FROM users u 
                WHERE token = #{token};""")
    fun findUser(@Param("token") token: AuthToken): User?

    // language=sql
    @Insert("""INSERT INTO users(name, host, token) 
               VALUES (#{name}, #{host}, #{token});""")
    fun insertUser(@Param("name") name: String,
                   @Param("host") host: String,
                   @Param("token") token: AuthToken)
}
package net.battleships.game.domain.mapper

import net.battleships.game.domain.model.Battlefield
import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.domain.model.IdOfUser
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.springframework.stereotype.Repository

@Mapper
@Repository
interface BattlefieldMapper {

    // language=sql
    @Select("""SELECT b.game_id gameId,
                      b.user_id userId,
                      b.arrangement arrangement
                 FROM battlefields b
                WHERE game_id = #{gameId}
                  AND user_id = #{userId};""")
    fun findBattlefield(@Param("gameId") gameId: IdOfGame,
                        @Param("userId") userId: IdOfUser): Battlefield?
}
package net.battleships.game.domain.mapper

import net.battleships.game.domain.model.Game
import net.battleships.game.domain.model.GameStatus
import net.battleships.game.domain.model.IdOfGame
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.apache.ibatis.annotations.Update
import org.springframework.stereotype.Repository
import java.util.Date

@Mapper
@Repository
interface GameMapper {

    // language=sql
    @Select("""SELECT g.id id,
                      g.player_id playerId,
                      g.opponent_id opponentId,
                      g.start_date startDate,
                      g.finish_date finishDate,
                      g.status status
                 FROM games g
                WHERE id = #{gameId};""")
    fun findGame(@Param("gameId") gameId: IdOfGame): Game?

    // language=sql
    @Update("""UPDATE games 
                  SET start_date = #{startDate}, 
                      status = #{status} 
                WHERE id = #{gameId};""")
    fun updateStartDateAndStatus(@Param("gameId") gameId: IdOfGame,
                                 @Param("startDate") startDate: Date,
                                 @Param("status") status: GameStatus)

    // language=sql
    @Update("""UPDATE games 
                  SET finish_date = #{finishDate}, 
                      status = #{status} 
                WHERE id = #{gameId};""")
    fun updateFinishDateAndStatus(@Param("gameId") gameId: IdOfGame,
                                  @Param("finishDate") finishDate: Date,
                                  @Param("status") status: GameStatus)
}
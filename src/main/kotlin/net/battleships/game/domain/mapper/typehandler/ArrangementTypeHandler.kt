package net.battleships.game.domain.mapper.typehandler

import net.battleships.game.domain.model.Arrangement
import org.apache.ibatis.type.BaseTypeHandler
import org.apache.ibatis.type.JdbcType
import java.sql.CallableStatement
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet

@Suppress("unused", "unchecked_cast")
class ArrangementTypeHandler: BaseTypeHandler<Arrangement>() {

    override fun getNullableResult(resultSet: ResultSet, columnName: String): Arrangement? {
        val array: Array<Array<Int>>? = resultSet.getArray(columnName)?.array as Array<Array<Int>>?

        return if (array == null) null else Arrangement(arrayToCoords(array))
    }

    override fun getNullableResult(resultSet: ResultSet, columnIndex: Int): Arrangement? {
        val array: Array<Array<Int>>? = resultSet.getArray(columnIndex)?.array as Array<Array<Int>>?

        return if (array == null) null else Arrangement(arrayToCoords(array))
    }

    override fun getNullableResult(callableStatement: CallableStatement, columnIndex: Int): Arrangement? {
        val array: Array<Array<Int>>? = callableStatement.getArray(columnIndex)?.array as Array<Array<Int>>?

        return if (array == null) null else Arrangement(arrayToCoords(array))
    }

    override fun setNonNullParameter(preparedStatement: PreparedStatement, index: Int, arrangement: Arrangement, jdbcType: JdbcType?) {
        val connection: Connection = preparedStatement.connection
        val array: java.sql.Array = connection.createArrayOf("INT", coordsToArray(arrangement.coords))

        preparedStatement.setArray(index, array)
    }

    private fun arrayToCoords(array: Array<Array<Int>>): List<List<Int>> {
        return array.asSequence()
                .map(Array<Int>::toList)
                .toList()
    }

    private fun coordsToArray(coords: List<List<Int>>): Array<Array<Int>> {
        return coords.asSequence()
                .map { it.toTypedArray() }
                .toList().toTypedArray()
    }
}
package net.battleships.game.domain.model

data class Arrangement(val coords: List<List<Int>>) {
    init {
        val rows: Int = coords.size
        val cols: Int = coords.asSequence()
                .map(List<Int>::size)
                .distinct()
                .sum()

        check(rows == cols) { "Rows count should be equal to cols count in every row" }
    }
}

data class Battlefield(val gameId: IdOfGame,
                       val userId: IdOfUser,
                       val arrangement: Arrangement)
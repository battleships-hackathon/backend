package net.battleships.game.domain.model

inline class IdOfUser(val id: Int)

inline class AuthToken(val token: String)

data class User(
        val id: IdOfUser,
        val name: String,
        val host: String,
        val token: AuthToken
)
package net.battleships.game.domain.model

import java.util.Date

inline class IdOfGame(val id: Int)

enum class GameStatus {
    NOT_STARTED, IN_PROGRESS, DRAW, PLAYER_WIN, OPPONENT_WIN;

    fun isNotStarted(): Boolean {
        return this == NOT_STARTED
    }

    fun isStarted(): Boolean {
        return this == IN_PROGRESS
    }

    fun isFinished(): Boolean {
        return this == DRAW || this == PLAYER_WIN || this == OPPONENT_WIN
    }
}

data class Game(val id: IdOfGame,
                val playerId: IdOfUser,
                val opponentId: IdOfUser,
                val startDate: Date?,
                val finishDate: Date?,
                val status: GameStatus)
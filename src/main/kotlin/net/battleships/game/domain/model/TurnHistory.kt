package net.battleships.game.domain.model

data class Point(val x: Int, val y: Int)

enum class TurnResult { HIT, MISS, TIMEOUT }

data class TurnHistory(val gameId: IdOfGame,
                       val userId: IdOfUser,
                       val number: Int,
                       val point: Point,
                       val result: TurnResult
) {
    constructor(gameId: IdOfGame, userId: IdOfUser, number: Int, abscissa: Int, ordinate: Int, result: TurnResult)
            : this(gameId, userId, number, Point(abscissa, ordinate), result)
}
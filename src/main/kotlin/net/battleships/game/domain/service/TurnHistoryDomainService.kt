package net.battleships.game.domain.service

import net.battleships.game.domain.mapper.TurnHistoryMapper
import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.domain.model.IdOfUser
import net.battleships.game.domain.model.Point
import net.battleships.game.domain.model.TurnHistory
import net.battleships.game.domain.model.TurnResult
import org.springframework.stereotype.Service

@Service
class TurnHistoryDomainService(private val turnHistoryMapper: TurnHistoryMapper) {

    fun listTurnHistory(gameId: IdOfGame): List<TurnHistory> {
        return turnHistoryMapper.listTurnHistory(gameId).toList()
    }

    fun saveTurnTimeOut(gameId: IdOfGame, userId: IdOfUser) {
        saveTurnHistory(gameId, userId, Point(0, 0), TurnResult.TIMEOUT)
    }

    fun saveTurnOnTime(gameId: IdOfGame, userId: IdOfUser, point: Point, result: TurnResult) {
        saveTurnHistory(gameId, userId, point, result)
    }

    private fun saveTurnHistory(gameId: IdOfGame, userId: IdOfUser, point: Point, result: TurnResult) {
        turnHistoryMapper.insertTurnHistory(gameId, userId, point.x, point.y, result)
    }
}
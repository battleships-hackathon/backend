package net.battleships.game.domain.service

import net.battleships.game.domain.mapper.UserMapper
import net.battleships.game.domain.model.AuthToken
import net.battleships.game.domain.model.User
import net.battleships.game.exception.ResourceNotFoundException
import net.battleships.game.utils.LoggerDelegate
import org.slf4j.Logger
import org.springframework.stereotype.Service

@Service
class UserDomainService(private val userMapper: UserMapper) {
    private val logger: Logger by LoggerDelegate()

    fun findUser(token: AuthToken): User {
        return userMapper.findUser(token)
                ?: throw ResourceNotFoundException("Unable to find user")
    }

    fun saveUser(name: String, host: String, token: AuthToken) {
        logger.info("Saving user, name: $name, host: $host, token: $token")

        userMapper.insertUser(name, host, token)
    }
}
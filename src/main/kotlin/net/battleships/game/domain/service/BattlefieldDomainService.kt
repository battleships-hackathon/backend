package net.battleships.game.domain.service

import net.battleships.game.domain.mapper.BattlefieldMapper
import net.battleships.game.domain.model.Battlefield
import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.domain.model.IdOfUser
import net.battleships.game.exception.ResourceNotFoundException
import org.springframework.stereotype.Service

@Service
class BattlefieldDomainService(private val battlefieldMapper: BattlefieldMapper) {

    fun findBattlefield(gameId: IdOfGame, userId: IdOfUser): Battlefield {
        return battlefieldMapper.findBattlefield(gameId, userId)
                ?: throw ResourceNotFoundException("Unable to find battlefield")
    }
}
package net.battleships.game.domain.service

import net.battleships.game.domain.mapper.GameMapper
import net.battleships.game.domain.model.Game
import net.battleships.game.domain.model.GameStatus
import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.exception.ResourceNotFoundException
import net.battleships.game.utils.LoggerDelegate
import net.battleships.game.utils.formatDate
import org.slf4j.Logger
import org.springframework.stereotype.Service
import java.util.Date

@Service
class GameDomainService(private val gameMapper: GameMapper) {
    private val logger: Logger by LoggerDelegate()

    fun findGame(gameId: IdOfGame): Game {
        return gameMapper.findGame(gameId)
                ?: throw ResourceNotFoundException("Game not found")
    }

    fun startGame(gameId: IdOfGame) {
        val current: Game = findGame(gameId)
        check(current.status.isStarted()) { "Game already started" }
        check(current.status.isFinished()) { "Game already finished" }

        val startDate = Date()
        logger.info("Updating game, id: $gameId, start date: ${startDate.formatDate()}, status: ${GameStatus.IN_PROGRESS}")

        gameMapper.updateStartDateAndStatus(gameId, startDate, GameStatus.IN_PROGRESS)
    }

    fun finishGame(gameId: IdOfGame, status: GameStatus) {
        val current: Game = findGame(gameId)
        check(current.status.isNotStarted()) { "Game not started yet" }
        check(current.status.isFinished()) { "Game already finished" }

        val finishDate = Date()
        logger.info("Updating game, id: $gameId, finish date: ${finishDate.formatDate()}, status: $status")

        gameMapper.updateFinishDateAndStatus(gameId, finishDate, status)
    }
}
package net.battleships.game.security.config

import net.battleships.game.security.token.TokenFilter
import net.battleships.game.security.token.TokenService
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
class SecurityConfig(private val tokenService: TokenService) : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        // @formatter:off
        http.cors().disable()
                .csrf().disable()
                .addFilterBefore(TokenFilter(tokenService), UsernamePasswordAuthenticationFilter::class.java)
                .authorizeRequests()
                    .antMatchers("/api/v1/signin").permitAll()
                    .anyRequest().authenticated()
        // @formatter:on
    }
}
package net.battleships.game.security.token

import net.battleships.game.domain.model.AuthToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

class TokenFilter(private val tokenService: TokenService) : GenericFilterBean() {
    override fun doFilter(request: ServletRequest, response: ServletResponse, filterChain: FilterChain) {
        val header: String? = (request as HttpServletRequest).getHeader("Authorization")
        val authentication: Authentication? =
                if (header != null) tokenService.getAuthentication(AuthToken(header)) else null

        SecurityContextHolder.getContext().authentication = authentication
        filterChain.doFilter(request, response)
    }
}
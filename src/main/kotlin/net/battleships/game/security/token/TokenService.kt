package net.battleships.game.security.token

import net.battleships.game.domain.model.AuthToken
import net.battleships.game.domain.service.UserDomainService
import net.battleships.game.utils.LoggerDelegate
import org.slf4j.Logger
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Service
import java.security.MessageDigest
import java.util.Base64
import java.util.UUID

@Service
class TokenService(private val userDomainService: UserDomainService) {
    private val logger: Logger by LoggerDelegate()

    private val digest: MessageDigest = MessageDigest.getInstance("SHA-256")

    fun getAuthentication(authToken: AuthToken): Authentication? {
        try {
            return Token(userDomainService.findUser(authToken))
        } catch (ex: IllegalStateException) {
            logger.error("Unable to get authentication", ex)
        }
        return null
    }

    fun generateToken(): AuthToken {
        val uuid: String = UUID.randomUUID().toString()
        val bytes: ByteArray = digest.digest(uuid.toByteArray())

        return AuthToken(Base64.getEncoder().encodeToString(bytes))
    }
}
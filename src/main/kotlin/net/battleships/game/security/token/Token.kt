package net.battleships.game.security.token

import net.battleships.game.domain.model.User
import org.springframework.security.authentication.AbstractAuthenticationToken

@Suppress("ConvertSecondaryConstructorToPrimary")
class Token : AbstractAuthenticationToken {
    private val user: User

    constructor(user: User) : super(listOf()) {
        this.user = user
        isAuthenticated = true
    }

    override fun getCredentials(): String = ""

    override fun getPrincipal(): String = user.name
}
package net.battleships.game.utils

import java.text.SimpleDateFormat
import java.util.Date

object DateFormatter {
    val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
}

fun Date.formatDate(): String {
    return DateFormatter.formatter.format(this)
}
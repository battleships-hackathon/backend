package net.battleships.game.logic.service

import net.battleships.game.domain.model.Game
import net.battleships.game.domain.model.GameStatus
import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.domain.model.IdOfUser
import net.battleships.game.domain.model.Point
import net.battleships.game.domain.model.TurnResult
import net.battleships.game.domain.service.GameDomainService
import net.battleships.game.domain.service.TurnHistoryDomainService
import net.battleships.game.logic.model.BoardState
import net.battleships.game.logic.model.GameState
import net.battleships.game.utils.LoggerDelegate
import org.slf4j.Logger
import org.springframework.stereotype.Service

@Service
class GameProcessService(
        private val gameStateService: GameStateService,
        private val turnTimerService: TurnTimerService,
        private val gameDomainService: GameDomainService,
        private val turnHistoryDomainService: TurnHistoryDomainService
) {
    private val logger: Logger by LoggerDelegate()

    fun startGame(gameId: IdOfGame) {
        val game: Game = gameDomainService.findGame(gameId)

        logger.info("Starting game, id: $gameId")
        gameDomainService.startGame(gameId)
        gameStateService.createGameState(game)

        processGame(gameId)
    }

    fun processTurn(gameId: IdOfGame, playerId: IdOfUser, point: Point) {
        fun getTurnResult(gameState: GameState, point: Point): TurnResult {
            return when (gameState.currentOpponentState[point]) {
                BoardState.SHIP -> TurnResult.HIT
                else -> TurnResult.MISS
            }
        }

        logger.info("Processing turn, game id: $gameId, player id: $playerId, point: $point")
        val gameState: GameState = gameStateService.findGameState(gameId)

        gameState.processTurn(playerId, point)
        turnTimerService.cancelTurnTimer(gameId)

        val result: TurnResult = getTurnResult(gameState, point)
        turnHistoryDomainService.saveTurnOnTime(gameId, playerId, point, result)

        logger.info("Turn processed, game id: $gameId, player id: $playerId, point: $point")

        processGame(gameId)
    }

    private fun processGame(gameId: IdOfGame) {
        logger.info("Processing game, id: $gameId")
        val gameState: GameState = gameStateService.findGameState(gameId)

        if (gameStateService.isGameOver(gameId)) {
            finishGame(gameId)
        }

        // TODO [EG]: send player game state

        turnTimerService.startTurnTimer(gameState.gameId) {
            turnHistoryDomainService.saveTurnTimeOut(gameState.gameId, gameState.currentPlayerId)
            processGame(gameId)
        }
        logger.info("Game processed, game id: $gameId")
    }

    private fun finishGame(gameId: IdOfGame) {
        logger.info("Finishing game, id: $gameId")

        val gameState: GameState = gameStateService.findGameState(gameId)

        val playerScore: Int = gameState.playerState.calculateScore()
        val opponentScore: Int = gameState.opponentState.calculateScore()

        val status: GameStatus = when {
            playerScore > opponentScore -> GameStatus.PLAYER_WIN
            playerScore < opponentScore -> GameStatus.OPPONENT_WIN
            else -> GameStatus.DRAW
        }

        gameDomainService.finishGame(gameId, status)
        logger.info("Game finished, id: $gameId")

        // TODO [EG]: notify players, game ended
    }
}
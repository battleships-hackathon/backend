package net.battleships.game.logic.service

import com.google.common.util.concurrent.ThreadFactoryBuilder
import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.utils.LoggerDelegate
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

@Service
class TurnTimerService(
        @Value("\${battleships.game.turn.timeout.seconds}") private val turnTimeoutSeconds: Long
) {
    private val logger: Logger by LoggerDelegate()

    private val turnTimerMap: ConcurrentHashMap<IdOfGame, ScheduledFuture<*>> = ConcurrentHashMap()
    private val turnTimerExecutor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor(
            ThreadFactoryBuilder()
                    .setDaemon(true)
                    .setNameFormat("turn-timer-executor")
                    .build()
    )

    fun startTurnTimer(gameId: IdOfGame, callback: () -> Unit) {
        logger.info("Starting turn timer, game id: $gameId")
        turnTimerMap[gameId] = turnTimerExecutor.schedule(callback, turnTimeoutSeconds, TimeUnit.SECONDS)
    }

    fun cancelTurnTimer(gameId: IdOfGame) {
        logger.info("Canceling turn timer, game id: $gameId")
        val turnTimer: ScheduledFuture<*> = turnTimerMap[gameId] ?: return

        turnTimer.cancel(true)
        turnTimerMap.remove(gameId)
    }
}
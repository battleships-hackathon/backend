package net.battleships.game.logic.service

import net.battleships.game.domain.model.Arrangement
import net.battleships.game.domain.model.Game
import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.domain.model.IdOfUser
import net.battleships.game.domain.service.BattlefieldDomainService
import net.battleships.game.domain.service.GameDomainService
import net.battleships.game.logic.model.GameState
import net.battleships.game.utils.LoggerDelegate
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.Date
import java.util.concurrent.ConcurrentHashMap

@Service
class GameStateService(
        private val gameDomainService: GameDomainService,
        private val battlefieldDomainService: BattlefieldDomainService,
        @Value("\${battleships.game.round.timeout.minutes}") private val roundTimeoutMinutes: Long
) {
    private val logger: Logger by LoggerDelegate()

    private val gameStateMap: ConcurrentHashMap<IdOfGame, GameState> = ConcurrentHashMap()

    fun findGameState(gameId: IdOfGame): GameState {
        return gameStateMap[gameId]
                ?: throw IllegalStateException("Unable to find game $gameId state")
    }

    fun createGameState(game: Game): GameState {
        logger.info("Creating game state, game id: ${game.id}")

        val (_, playerId: IdOfUser, playerArrangement: Arrangement) =
                battlefieldDomainService.findBattlefield(game.id, game.playerId)
        val (_, opponentId: IdOfUser, opponentArrangement: Arrangement) =
                battlefieldDomainService.findBattlefield(game.id, game.opponentId)

        return put(GameState(game.id, playerId, playerArrangement, opponentId, opponentArrangement))
    }

    fun isGameOver(gameId: IdOfGame): Boolean {
        val gameState: GameState = findGameState(gameId)
        return gameState.isAnyPlayerDestroyed() || isTimeExpired(gameId)
    }

    private fun isTimeExpired(gameId: IdOfGame): Boolean {
        val game: Game = gameDomainService.findGame(gameId)
        if (game.startDate == null) {
            return false
        }

        val gameTime: Long = Date().time - game.startDate.time
        return roundTimeoutMinutes * 60 * 1000 - gameTime <= 0
    }

    private fun put(gameState: GameState): GameState {
        gameStateMap[gameState.gameId] = gameState
        return gameState
    }
}
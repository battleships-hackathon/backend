package net.battleships.game.logic.model

import net.battleships.game.domain.model.Arrangement
import net.battleships.game.domain.model.IdOfGame
import net.battleships.game.domain.model.IdOfUser
import net.battleships.game.domain.model.Point

class GameState {
    val gameId: IdOfGame
    val playerState: PlayerState
    val opponentState: PlayerState

    var currentPlayerId: IdOfUser
        private set
    val currentOpponentState: PlayerState
        get() = if (currentPlayerId == playerState.playerId) opponentState else playerState

    constructor(gameId: IdOfGame, playerState: PlayerState, opponentState: PlayerState) {
        this.gameId = gameId
        this.playerState = playerState
        this.opponentState = opponentState
        this.currentPlayerId = playerState.playerId
    }

    constructor(gameId: IdOfGame, playerId: IdOfUser, playerArrangement: Arrangement, opponentId: IdOfUser, opponentArrangement: Arrangement)
            : this(gameId, PlayerState(playerId, playerArrangement), PlayerState(opponentId, opponentArrangement))

    fun isAnyPlayerDestroyed(): Boolean {
        return playerState.isShipsDestroyed() || opponentState.isShipsDestroyed()
    }

    fun processTurn(playerId: IdOfUser, point: Point) {
        check(playerId == currentPlayerId) { "Game $gameId has different current player: $currentPlayerId" }

        currentOpponentState.updateBoard(point)
        currentPlayerId = currentOpponentState.playerId
    }
}
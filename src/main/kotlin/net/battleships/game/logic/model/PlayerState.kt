package net.battleships.game.logic.model

import net.battleships.game.domain.model.Arrangement
import net.battleships.game.domain.model.IdOfUser
import net.battleships.game.domain.model.Point
import net.battleships.game.domain.model.TurnHistory
import net.battleships.game.domain.model.TurnResult

@Suppress("ConvertSecondaryConstructorToPrimary")
class PlayerState {
    val playerId: IdOfUser
    private val board: GameBoard

    constructor(playerId: IdOfUser, arrangement: Arrangement, history: List<TurnHistory> = listOf()) {
        this.playerId = playerId
        this.board = GameBoard(arrangement)

        for (turn: TurnHistory in history) {
            when (turn.result) {
                TurnResult.HIT -> board[turn.point] = BoardState.HIT
                TurnResult.MISS -> board[turn.point] = BoardState.MISS
                else -> Unit
            }
        }
    }

    operator fun get(point: Point): BoardState = board[point]

    fun isShipsDestroyed(): Boolean = board.isShipsDestroyed()

    fun calculateScore(): Int {
        fun getBoardStateScore(state: BoardState): Int {
            return when (state) {
                BoardState.HIT -> -2
                BoardState.MISS -> +1
                BoardState.SHIP -> +3
                BoardState.EMPTY -> 0
            }
        }

        return board.asSequence()
                .sumBy(::getBoardStateScore)
    }

    fun updateBoard(point: Point) {
        fun transformBoardState(point: Point): BoardState {
            return when (board[point]) {
                BoardState.SHIP, BoardState.HIT -> BoardState.HIT
                BoardState.EMPTY, BoardState.MISS -> BoardState.MISS
            }
        }

        board[point] = transformBoardState(point)
    }

}
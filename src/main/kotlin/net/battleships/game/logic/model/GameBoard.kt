package net.battleships.game.logic.model

import net.battleships.game.domain.model.Arrangement
import net.battleships.game.domain.model.Point

enum class BoardState { EMPTY, SHIP, MISS, HIT }

@Suppress("ConvertSecondaryConstructorToPrimary")
class GameBoard : Iterable<BoardState> {
    private val board: MutableList<MutableList<BoardState>>

    constructor(arrangement: Arrangement) {
        fun transform(cell: Int): BoardState {
            return if (cell == 0) BoardState.EMPTY else BoardState.SHIP
        }

        fun transform(row: List<Int>): MutableList<BoardState> {
            return row.asSequence().map(::transform).toMutableList()
        }

        this.board = arrangement.coords.asSequence()
                .map(::transform)
                .toMutableList()
    }

    operator fun get(point: Point): BoardState {
        checkOutOfBound(point)
        return board[point.x][point.y]
    }

    operator fun set(point: Point, value: BoardState) {
        checkOutOfBound(point)
        board[point.x][point.y] = value
    }

    override fun iterator(): Iterator<BoardState> {
        return board.flatten().iterator()
    }

    fun isShipsDestroyed(): Boolean {
        return asSequence().none { state -> state == BoardState.SHIP }
    }

    private fun checkOutOfBound(point: Point) {
        val rows: Int = board.size
        val cols: Int = board[point.x].size

        check(point.x in 0..rows) { "Point $point is out of bound" }
        check(point.y in 0..cols) { "Point $point is out of bound" }
    }
}